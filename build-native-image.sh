cp build/libs/*-all.jar  build/libs/the-app.jar
docker run --rm --name graal -v $(pwd):/working oracle/graalvm-ce:1.0.0-rc8 \
    /bin/bash -c "java -cp /working/build/libs/the-app.jar \
            io.micronaut.graal.reflect.GraalClassLoadingAnalyzer \
            /working/build/reflect.json \
        ; \
        native-image --no-server \
             --class-path /working/build/libs/the-app.jar \
			 -H:ReflectionConfigurationFiles=/working/build/reflect.json \
			 -H:EnableURLProtocols=http \
			 -H:IncludeResources='logback.xml|application.yml|META-INF/services/*.*' \
			 -H:Name=micronaut-app \
			 -H:Class=hello.world.java.Application \
			 -H:+ReportUnsupportedElementsAtRuntime \
			 -H:+AllowVMInspection \
			 --rerun-class-initialization-at-runtime='sun.security.jca.JCAUtil$CachedSecureRandomHolder',javax.net.ssl.SSLContext \
			 --delay-class-initialization-to-runtime=io.netty.handler.codec.http.HttpObjectEncoder,io.netty.handler.codec.http.websocketx.WebSocket00FrameEncoder,io.netty.handler.ssl.util.ThreadLocalInsecureRandom \
			 -H:-UseServiceLoaderFeature \
		; \
		cp micronaut-app /working/build"


